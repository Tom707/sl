import pandas as pd
import matplotlib.pyplot as plt
data = pd.read_csv("https://bitbucket.org/Tom707/sl/downloads/Salary_Data.csv")
data.head()
data.isnull().sum()
x = data['YearsExperience'].values
y = data['Salary'].values
x = x.reshape(len(x),1)
y = y.reshape(len(y),1)
from sklearn.model_selection import train_test_split
xtrain,xtest,ytrain,ytest = train_test_split(x,y,test_size=0.20)
from sklearn.linear_model import LinearRegression
model = LinearRegression()
model.fit(xtrain,ytrain)
ypred = model.predict(xtest)
ypred
ytest
from sklearn.metrics import r2_score
r = r2_score(ytest,ypred)
r
plt.scatter(xtrain,ytrain,color="purple")
plt.plot(xtrain,model.predict(xtrain),color="red")
plt.show()
plt.savefig("graph.png")
plt.scatter(xtest,ytest,color="purple")
plt.plot(xtest,model.predict(xtest),color="red")
plt.show()
m = model.coef_
c = model.intercept_
print(m,c)
model.predict([[20.9]])
m * 20.9 + c
